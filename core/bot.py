import os
import datetime
import disnake

from typing import List
from disnake import Activity, ActivityType, Intents, Message
from disnake.ext.commands import InteractionBot, Bot,  when_mentioned_or
from disnake.ext.tasks import loop

from database import Database
from constants import BOT_TOKEN


class BohtBot(Bot):
    def __init__(self, **kwargs):
        print('prefix', kwargs.get('prefix'))
        super().__init__(
            command_prefix=when_mentioned_or(kwargs.get('prefix')),
            owner_ids=kwargs.get('owner_ids'),
            test_guilds=kwargs.get('test_guilds'),
            intents=Intents.all(),
            case_insensitive=True,
            strip_after_prefix=True,
            allowed_mentions=disnake.AllowedMentions.none(),
            help_command=None,
        )

        self.launch_time = datetime.datetime.utcnow()
        self.COGS: list = []
        self.time_limit = 120
        self.disnakecolor = 0x36393F
        self.hiber = False
        self.data = {}

    @property
    def invite_url(self):
        return f'https://discord.com/api/oauth2/authorize?client_id=' \
               f'{self.user.id}&permissions=8&scope=bot%20applications.commands'

    def setup(self) -> None:
        print('Cogs:\n-----------------------------------')
        print(', '.join(self.COGS))
        for filename in self.COGS:
            if filename.endswith('.py'):
                self.load_extension(f'{filename[:-3]}')
            else:
                self.load_extension(filename)
        print(f'Loaded Cogs Successfully! Total Cogs: {len(self.COGS)}\n-----------------------------------')

    def run(self) -> None:
        # print('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n')
        print('Setting up...')
        print('-----------------------------------')
        self.setup()
        print('Running the bot...')
        print('-----------------------------------')
        super().run(BOT_TOKEN, reconnect=True)

    # async def connect_database(self):
    #     self.db = await Database.create()


class CogsLoader:
    def __init__(self, cogs_types: [str]):
        self.cogs_types = cogs_types

    def load(self, bot: BohtBot):
        for cog_type in self.cogs_types:
            for file in os.listdir(f'./cogs/{cog_type}'):
                if file.endswith('.py'):
                    extension = file[:-3]
                    try:
                        bot.COGS.append(f'cogs.{cog_type}.{extension}')
                        print(f'Loaded extension "{extension}"')
                    except Exception as e:
                        exception = f'{type(e).__name__}: {e}'
                        print(f'Failed to load extension {extension}\n{exception}')



