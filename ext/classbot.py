import os
import sqlite3
import discord
from discord.ext import commands


class Bot(commands.Bot):
    def __init__(self):
        super().__init__(
            command_prefix='?',
            description='zaGadka bot',
            intents=discord.Intents().all(),
            case_insensitive=True,
            strip_after_prefix=True,
        )
        self.db = sqlite3.connect('database/zagadka.db')
        self.dbcursor = self.db.cursor()

        for filename in os.listdir('./cogs'):
            if filename.endswith('.py'):
                self.load_extension(f'cogs.{filename[:-3]}')

        @updateactivity.before_loop
        async def wait_for_ready(self):
            """Waits until the bot is ready"""
            await self.wait_until_ready()

