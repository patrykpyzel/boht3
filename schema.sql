CREATE TABLE IF NOT EXISTS guild (
    guildId TEXT,
    welcome TEXT,
    goodbye TEXT,
    prefix TEXT,
    language TEXT);

CREATE TABLE IF NOT EXISTS snipe (
    id BIGINT PRIMARY KEY,
    channel_id BIGINT,
    user_id BIGINT,
    message_content TEXT,
    create_epoch DATE
);