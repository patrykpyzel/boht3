import disnake
from disnake.ext import commands
from core.constants import BOT_TOKEN


class ReadyEvent(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.app_info = None

    @commands.Cog.listener()
    async def on_ready(self):
        """
        This event is called every time the bot connects or resumes connection.
        """

        # await self.bot.change_presence(
        #     activity=disnake.Game(f'with {len(self.bot.guilds)} servers | -help | {1}'),
        #     afk=True)
        # print('-' * 10)
        # self.bot.app_info = await self.bot.application_info()
        print(
            f'Logged in as: {self.bot.user.name}\n'
            f'User id: {self.bot.user}\n'
            f'Using discord.py version: {disnake.__version__}\n')
        # print('-' * 10)


def setup(bot):
    bot.add_cog(ReadyEvent(bot))
