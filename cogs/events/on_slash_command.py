import disnake
from disnake.ext import commands
from core.constants import BOT_TOKEN


class SlashCommandEvent(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.app_info = None

    @commands.Cog.listener()
    async def on_slash_command(self, command):
        # print(command)
        # print(f'{datetime.utcnow()} on_guild_join {guild.name}, {len(guild.members)}')
        pass


def setup(bot):
    bot.add_cog(SlashCommandEvent(bot))
