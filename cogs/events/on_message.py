import disnake
from disnake.ext import commands
from core.constants import BOT_TOKEN


class MessageEvent(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.app_info = None

    @commands.Cog.listener()
    async def on_message(self, message):
        # print(f'{datetime.utcnow()} on_guild_join {guild.name}, {len(guild.members)}')
        # print(message)
        # await self.bot.process_commands(message)
        pass


def setup(bot):
    bot.add_cog(MessageEvent(bot))
