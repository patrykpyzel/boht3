import disnake
from disnake import ApplicationCommandInteraction, Option, OptionType
from disnake.ext import commands


class GeneralCommands(commands.Cog, name='general commands'):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(
        name='test',
        description='test'
    )
    async def test(self, interaction: ApplicationCommandInteraction) -> None:
        """Slash command for testing"""
        await interaction.send('test!')

    @commands.slash_command(
        name='invite',
        description='Przez podany link, prawidłowo zaprosisz mnie na serwer 8)',
    )
    async def invite(self, inter: ApplicationCommandInteraction) -> None:
        """Slash command for invite bot"""
        await inter.response.send_message(self.bot.invite_url)

    @commands.command(
        name='invite',
        description='Przez podany link, prawidłowo zaprosisz mnie na serwer 8)',
        aliases=['i', 'inv', 'zaproszenie']
    )
    async def invite_(self, ctx) -> None:
        """Slash command for invite bot"""
        await ctx.reply(self.bot.invite_url)

    @commands.slash_command(
        name='remind',
        description='Ustal za jaki czas mam Ci coś przypomnieć'
    )
    async def remind(self, inter: ApplicationCommandInteraction) -> None:
        """Slash command for reminder"""

        await inter.send(self.bot.invite_url)


def setup(bot):
    bot.add_cog(GeneralCommands(bot))
