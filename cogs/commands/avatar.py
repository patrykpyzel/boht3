from typing import Union
import disnake
from disnake import ApplicationCommandInteraction, Option, OptionType
from disnake.ext import commands


class AvatarCommands(commands.Cog, name='Avatar commands'):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(
        name='avatar',
        description='powiększ awatar swój lub któregoś użytkownika'
    )
    async def avatar(self,
                     inter: ApplicationCommandInteraction,
                     user: Union[disnake.User, None] = None) -> None:
        # guild: Union[bool, None] = None) -> None:
        """Slash command for avatars"""
        if not user:
            user = inter.author

        embed = disnake.Embed(
            title=f'{user}',
            description=f'[avatar url]({user.display_avatar.url}) || [invite bot]({self.bot.invite_url})',
        )
        embed.set_image(url=user.display_avatar.url)
        # emb.set_author(name=inter.author, icon_url=inter.author.display_avatar.url)
        await inter.response.send_message(embed=embed)

    @commands.command(
        name='avatar',
        description='powiększ awatar swój lub któregoś użytkownika',
        aliases=['a', 'av', 'avk']
    )
    async def avatar_(self, ctx, *, user: Union[disnake.User, None]) -> None:
        """Legacy command for avatars"""
        if not user:
            user = ctx.author

        embed = disnake.Embed(
            title=f'{user}',
            description=f'[avatar url]({user.display_avatar.url}) || [invite bot]({self.bot.invite_url})',
        )
        embed.set_image(url=user.display_avatar.url)
        # embed.set_author(name=ctx.author, icon_url=ctx.author.display_avatar.url)
        await ctx.reply(embed=embed)

    @staticmethod
    def enhance_image(url, size=2048):
        return url.split('=')[0] + f'={size}'


def setup(bot):
    bot.add_cog(AvatarCommands(bot))
