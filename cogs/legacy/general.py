import disnake
from disnake import ApplicationCommandInteraction, Option, OptionType
from disnake.ext import commands


class GeneralLegacy(commands.Cog, name='Avatar commands'):
    def __init__(self, bot):
        self.bot = bot



    async def age(self, ctx, name: str) -> None:
        async with self.bot.client.get(url=f"https://api.agify.io/?name={name}") as resp:
            json = await resp.json()

        await ctx.em(f"The bot thinks you are {json['age']} years old!")

    @staticmethod
    def enhance_image(url, size=2048):
        return url.split('=')[0] + f'={size}'


def setup(bot):
    bot.add_cog(GeneralLegacy(bot))
