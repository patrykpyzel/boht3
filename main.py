from core import BohtBot, CogsLoader, BOT_PREFIX, OWNER_IDS

if __name__ == '__main__':
    bot = BohtBot(prefix=BOT_PREFIX,
                  owner_ids=OWNER_IDS,
                  test_guilds=[957264333509898312],
                  case_insensitive=True,
                  strip_after_prefix=True,
                  help_command=None,
                  )
    cogs_loader = CogsLoader(['commands', 'events'])
    cogs_loader.load(bot)
    bot.run()
